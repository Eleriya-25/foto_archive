import os.path

from aiohttp import web
import aiofiles
import asyncio
import logging
import argparse


ROOT_DIR = os.path.dirname(os.path.abspath(__file__))
DEFAULT_PHOTOS_PATH = f"{ROOT_DIR}/test_photos/"
DEFAULT_SLEEP_IN_SECONDS = None
CHUNK_SIZE = 100 * 1024


async def archive_photos(folder_path, cwd):
    process = await asyncio.create_subprocess_shell(
        f"zip -r - {folder_path}",
        stdout=asyncio.subprocess.PIPE,
        stderr=asyncio.subprocess.PIPE,
        cwd=cwd,
    )

    try:
        while True:
            if process.stdout.at_eof():
                break

            chunk = await process.stdout.read(CHUNK_SIZE)
            yield chunk

    finally:
        process.kill()
        await process.communicate()


async def archive(request):
    archive_hash = request.match_info["archive_hash"]
    folder_path = f"{photos_path}{archive_hash}"

    if not os.path.exists(folder_path):
        return web.Response(status=404, text="Архив не существует или был удален")

    response = web.StreamResponse()
    response.headers["Content-Disposition"] = 'attachment; filename="archive.zip"'

    await response.prepare(request)

    try:
        async for chunk in archive_photos(folder_path, photos_path):
            logging.info("Sending archive chunk ...")
            await response.write(chunk)

            if sleep_in_seconds:
                await asyncio.sleep(sleep_in_seconds)

    except asyncio.CancelledError:
        logging.error("Download was interrupted")
        raise

    return response


async def handle_index_page(request):
    async with aiofiles.open('index.html', mode='r') as index_file:
        index_contents = await index_file.read()
    return web.Response(text=index_contents, content_type='text/html')


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Сервер фотографий')
    parser.add_argument("--log_mode", dest="log_mode", choices=["on", "off"])
    parser.add_argument("--sleep", dest="sleep_in_seconds", const=None)
    parser.add_argument("--photos", dest="photos_path", const=None)
    args = parser.parse_args()

    if args.log_mode == "on":
        logging.basicConfig(level=logging.INFO)
    else:
        logging.disable()

    sleep_in_seconds = args.sleep_in_seconds or DEFAULT_SLEEP_IN_SECONDS
    photos_path = args.photos_path or DEFAULT_PHOTOS_PATH

    app = web.Application()
    app.add_routes([
        web.get('/', handle_index_page),
        web.get('/archive/{archive_hash}/', archive),
    ])
    web.run_app(app)
